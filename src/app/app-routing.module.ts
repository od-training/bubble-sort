import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'bubble-sort', pathMatch: 'full' },
  {
    path: 'bubble-sort',
    loadChildren: './bubble-sort/bubble-sort.module#BubbleSortModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
