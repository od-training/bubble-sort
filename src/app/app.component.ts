import { Component } from '@angular/core';

@Component({
  selector: 'bbs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bubbleSort';
}
