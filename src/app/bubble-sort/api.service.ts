import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, map, switchMap } from 'rxjs/operators';

import { FOV } from './bubble-sort.types';

@Injectable({
  providedIn: 'root'
})
export class APIService {
  fovs: Observable<FOV[]>;
  private requestTrigger = new Subject();

  constructor(httpclient: HttpClient) {
    this.fovs = this.requestTrigger.pipe(
      debounceTime(350),
      switchMap(() => httpclient.get<FOV[]>('http://localhost:3000/fovs')),
      map(fovs => findRandomFovs(fovs))
    );
  }

  updateRequest() {
    this.requestTrigger.next();
  }
}

export function findRandomFovs(fovs: FOV[], count: number = 4) {
  const randomIndices: number[] = [];

  let random: number;
  for (let x = 0; x < count; x++) {
    do {
      random = Math.floor(Math.random() * fovs.length);
    } while (randomIndices.find(index => index === random));
    randomIndices.push(random);
  }

  const randomFovs: FOV[] = [];
  randomIndices.forEach(index => randomFovs.push(fovs[index]));
  return randomFovs;
}
