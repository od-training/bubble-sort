import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BubbleSortComponent } from './bubble-sort.component';

const routes: Routes = [{ path: '', component: BubbleSortComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BubbleSortRoutingModule {}
