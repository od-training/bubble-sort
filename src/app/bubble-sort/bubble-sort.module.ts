import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatSelectModule
} from '@angular/material';

import { AnnotationComponent } from './annotation/annotation.component';
import { BubbleSortRoutingModule } from './bubble-sort-routing.module';
import { BubbleSortComponent } from './bubble-sort.component';
import { FieldOfViewComponent } from './field-of-view/field-of-view.component';
import { FovContainerComponent } from './fov-container/fov-container.component';
import { ImageToolsComponent } from './image-tools/image-tools.component';
import { ParametersComponent } from './parameters/parameters.component';
import { ReusableExampleModule } from './reusable-example/reusable-example.module';
import { ToolsComponent } from './tools/tools.component';

@NgModule({
  declarations: [
    BubbleSortComponent,
    ToolsComponent,
    AnnotationComponent,
    ParametersComponent,
    ImageToolsComponent,
    FovContainerComponent,
    FieldOfViewComponent
  ],
  imports: [
    CommonModule,
    BubbleSortRoutingModule,
    ReactiveFormsModule,
    ReusableExampleModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule
  ]
})
export class BubbleSortModule {}
