export interface FOV {
  id: string;
  channel1Url: string;
  channel2Url: string;
  metadata: {};
}

export interface Mask {
  fovId: string;
  url: string;
}

export interface UserFOV extends FOV {
  activeChannel: string;
}

export interface UserOptions {
  annotations: Annotations;
  parameters: MaskParameters;
}

export interface Annotations {
  channels: string;
}

export interface MaskParameters {}
