import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldOfViewComponent } from './field-of-view.component';

describe('FieldOfViewComponent', () => {
  let component: FieldOfViewComponent;
  let fixture: ComponentFixture<FieldOfViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldOfViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldOfViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
