import { Component, Input } from '@angular/core';

import { UserFOV } from '../bubble-sort.types';

@Component({
  selector: 'bbs-field-of-view',
  templateUrl: './field-of-view.component.html',
  styleUrls: ['./field-of-view.component.css']
})
export class FieldOfViewComponent {
  @Input() fov: UserFOV | undefined;
  constructor() {}
}
