import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FovContainerComponent } from './fov-container.component';

describe('FovContainerComponent', () => {
  let component: FovContainerComponent;
  let fixture: ComponentFixture<FovContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FovContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FovContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
