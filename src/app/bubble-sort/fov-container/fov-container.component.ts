import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { UserFOV } from '../bubble-sort.types';
import { ViewPortService } from '../view-port.service';

@Component({
  selector: 'bbs-fov-container',
  templateUrl: './fov-container.component.html',
  styleUrls: ['./fov-container.component.css']
})
export class FovContainerComponent {
  fovs: Observable<UserFOV[]>;
  constructor(vps: ViewPortService) {
    this.fovs = vps.userFovs;
  }
}
