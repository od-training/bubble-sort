import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'bbs-image-tools',
  templateUrl: './image-tools.component.html',
  styleUrls: ['./image-tools.component.css']
})
export class ImageToolsComponent {
  @Output() imageUpdate = new EventEmitter<void>();

  requestImages() {
    this.imageUpdate.emit();
  }
}
