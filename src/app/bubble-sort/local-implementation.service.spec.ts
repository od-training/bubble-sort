import { TestBed } from '@angular/core/testing';

import { LocalImplementationService } from './local-implementation.service';

describe('LocalImplementationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalImplementationService = TestBed.get(LocalImplementationService);
    expect(service).toBeTruthy();
  });
});
