import { Injectable } from '@angular/core';
import { of } from 'rxjs';

import { DependentServiceExample } from './reusable-example/shared.types';

@Injectable({
  providedIn: 'root'
})
export class LocalImplementationService implements DependentServiceExample {
  myProp1 = 'hello';
  constructor() {}

  myMethod(testString: string) {
    return of(testString);
  }
}
