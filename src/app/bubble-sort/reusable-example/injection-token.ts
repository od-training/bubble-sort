import { InjectionToken } from '@angular/core';

import { DependentServiceExample } from './shared.types';

export const ReusableHook = new InjectionToken<DependentServiceExample>(
  'ReusableHook'
);
