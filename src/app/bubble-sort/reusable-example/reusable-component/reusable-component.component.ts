import { Component, Inject } from '@angular/core';

import { ReusableHook } from '../injection-token';
import { DependentServiceExample } from '../shared.types';

@Component({
  selector: 'bbs-reusable-component',
  templateUrl: './reusable-component.component.html',
  styleUrls: ['./reusable-component.component.css']
})
export class ReusableComponentComponent {
  // PROBLEM: How do I make this reusable when there is
  // an app specific service dependency?
  // A: Injection Tokens are one solution
  constructor(@Inject(ReusableHook) service: DependentServiceExample) {
    console.log('service value', service.myProp1);
  }
}
