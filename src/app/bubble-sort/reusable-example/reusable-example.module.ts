import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReusableComponentComponent } from './reusable-component/reusable-component.component';

@NgModule({
  declarations: [ReusableComponentComponent],
  exports: [ReusableComponentComponent],
  imports: [CommonModule]
})
export class ReusableExampleModule {}
