import { Observable } from 'rxjs';

export interface DependentServiceExample {
  myProp1: string;
  myMethod: (param1: string) => Observable<string>;
}
