import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {
  toolingInteractions = this.fb.group({
    annotations: this.fb.group({
      channels: ['grayscale']
    }),
    parameters: this.fb.group({})
  });

  constructor(private fb: FormBuilder) {}
}
