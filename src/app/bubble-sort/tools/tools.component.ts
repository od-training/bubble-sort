import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { APIService } from '../api.service';
import { ToolsService } from '../tools.service';

@Component({
  selector: 'bbs-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.css']
})
export class ToolsComponent {
  annotationForm: FormGroup;
  parametersForm: FormGroup;
  constructor(private apiService: APIService, toolsService: ToolsService) {
    this.annotationForm = toolsService.toolingInteractions.get(
      'annotations'
    ) as FormGroup;
    this.parametersForm = toolsService.toolingInteractions.get(
      'parameters'
    ) as FormGroup;
  }

  updateImages() {
    this.apiService.updateRequest();
  }
}
