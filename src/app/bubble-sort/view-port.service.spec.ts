import { TestBed } from '@angular/core/testing';

import { ViewPortService } from './view-port.service';

describe('ViewPortService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewPortService = TestBed.get(ViewPortService);
    expect(service).toBeTruthy();
  });
});
