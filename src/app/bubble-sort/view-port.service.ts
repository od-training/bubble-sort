import { Injectable } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { APIService } from './api.service';
import { FOV, UserFOV, UserOptions } from './bubble-sort.types';
import { ToolsService } from './tools.service';

@Injectable({
  providedIn: 'root'
})
export class ViewPortService {
  userFovs: Observable<UserFOV[]>;

  constructor(apiService: APIService, toolsService: ToolsService) {
    const userChanges = toolsService.toolingInteractions.valueChanges.pipe(
      startWith(toolsService.toolingInteractions.value)
    );

    this.userFovs = combineLatest(apiService.fovs, userChanges).pipe(
      map(([fovs, userChoices]: [FOV[], UserOptions]) =>
        processFovs(fovs, userChoices)
      )
    );
  }
}

export function processFovs(fovs: FOV[], userChanges: UserOptions): UserFOV[] {
  return fovs.map(fov => {
    return { ...fov, activeChannel: userChanges.annotations.channels };
  });
}
